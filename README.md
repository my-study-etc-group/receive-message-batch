### 前提

[send-message-api](https://gitlab.com/my-study-etc-group/send-message-api)でSQSにキューイングされたメセージを取得する。
キューイングするところまでは上記のリポジトリ参照。

### 起動

#### localstackから取得する場合

```bash
export APP_ENDPOINT_URL=http://localhost:4566
export COPILOT_QUEUE_URI=http://localhost:4566/000000000000/sample-que
./gradlew run
```

localstackの場合通常エンドポイントは`http://localhost:4566`であるはず。  
キューのURLは次のコマンドで確認できる。

```bash
aws --endpoint-url=http://localhost:4566 sqs list-queues
```

#### AWSから取得する場合

```bash
export COPILOT_QUEUE_URI=https://sqs.ap-northeast-1.amazonaws.com/xxxxxxx/sample-que
./gradlew run
```

エンドポイントは設定しない。  
キューのURLは適宜変更する。

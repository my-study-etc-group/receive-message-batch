FROM gradle:7.3.2-jdk17 as build
WORKDIR /workspace/app

COPY settings.gradle .
COPY build.gradle .
COPY src src
RUN gradle build

FROM openjdk:17-jdk-slim

WORKDIR /workspace/app

COPY --from=build /workspace/app/build/libs/receive-message-batch-all.jar ./

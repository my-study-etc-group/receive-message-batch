package com.example.receivemessagebatch;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReceiveMessageBatch {
    public static void main(String[] args) {
        String queueUrl = System.getenv("COPILOT_QUEUE_URI");
        log.info("SQS URL is => " + queueUrl);

        String endpoint = System.getenv("APP_ENDPOINT_URL");
        log.info("ENDPOINT URL is => " + endpoint);

        AmazonSQS client = getClient(endpoint);

        ReceiveMessageRequest request =
            new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withWaitTimeSeconds(20)
                .withMaxNumberOfMessages(10);

        while (true) {
            log.info("start message request");
            List<Message> messages = client.receiveMessage(request).getMessages();

            if (messages.size() == 0) {
                log.info("no messages");
            }

            for (Message m : messages) {
                log.info("message is " + m);
                client.deleteMessage(queueUrl, m.getReceiptHandle());
            }
        }
    }

    private static AmazonSQS getClient(String endpoint) {
        if (endpoint == null) {
            return AmazonSQSClientBuilder.defaultClient();
        } else {
            AwsClientBuilder.EndpointConfiguration endpointConfig =
                new AwsClientBuilder.EndpointConfiguration(endpoint,
                    Regions.AP_NORTHEAST_1.getName());
            return AmazonSQSClientBuilder.standard()
                .withEndpointConfiguration(endpointConfig)
                .build();
        }
    }
}
